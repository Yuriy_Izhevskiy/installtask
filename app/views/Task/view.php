<?php
use fw\libs\helpers\Html;
?>
<div class="row">
    <ol class="breadcrumb">
        <li><a href="/">Список всех задач</a></li>
        <li class="active">Просмотр</li>
    </ol>

    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-body">
                    <?php foreach ($findDataById as $res ):?>
                    <div class="col-md-5"><img class="img-rounded" title="изображение" src="<?= Html::encode($res['img'])?>"><br></div>
                    <h3 class="panel-title">
                        <div class="form-group">
                            <b>Ваше имя:</b> <?= Html::encode($res['name'])?>
                        </div>
                        <div class="form-group">
                            <b>Ваше email:</b> <?= Html::encode($res['email'])?>
                        </div>
                        <div class="form-group">
                            <b>Статус:</b> <?= Html::encode($res['status_name'])?>
                        </div>

                    <hr>
                    <div class="col-md-7">
                        <em>
                        <?= Html::encode($res['task'])?>
                        </em>
                    </div>
                <?php endforeach;?>

            </div>

        </div>
    </div>
</div>

