<?php
namespace app\controllers;

use fw\core\base\Controller;
use fw\libs\Message;

class AppController extends Controller
{
    const CONTROLLER_TASK =  'Task';
    const ACTION_EDIT = 'edit';

    public function __construct($route)
    {
        parent::__construct($route);
        $this->accessAuthorization($this->route);

    }

    public function accessAuthorization($role)
    {
        if ($role['controller'] == self::CONTROLLER_TASK && $role['action'] == self::ACTION_EDIT) {
            if (is_null($this->getSessionAdmin())) {
                Message::MessageSendRole(4, 'Недостаточно прав для совершения операции!');
            }
        }
    }

    public function getSessionAdmin()
    {
        return $_SESSION['admin'] ? $_SESSION['admin'] : null;
    }

}