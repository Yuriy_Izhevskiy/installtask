<?php
namespace app\controllers;

use app\models\Task;
use fw\core\libs;
use fw\libs\Message;
use fw\libs\Validator;


class TaskController extends AppController
{
    public $layout = 'main';

    public function newAction(){}

    /**
     *
     */
    public function addAction()
    {
        $this->layout = false;

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $model = new Task();
            $validator = new Validator();

            $name = $validator->clean($_POST['user_name']);
            $task = $validator->clean($_POST['user_task']);
            $email = $validator->clean($_POST['user_email']);

            $validationFormData = $model->validationFormAddData($name, $email, $task);
            $validationImage = $model->validationImage($_FILES['user_img']);

            if ($validationFormData && $validationImage == true) {
                $saveImage = $model->saveImage($_FILES['user_img']);
                $getImagePath = $model->pathImgForDB();

                $savingDataDatabase = $model->insert($name, $email, $task, $getImagePath);
                if ($saveImage && $savingDataDatabase) {
                    Message::MessageSendSuccess(1, 'Данные успешно соxранены!');
                } else {
                    Message::MessageSendError(2, 'Данные не удалось сохранить. Попробуйте снова!');
                }
            }
        }
    }

    public function editAction()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $model = new Task();
            $validator = new Validator();

            $id = $model->validateInt($_POST['id']);
            $statusId = $model->validateInt($_POST['status']);

            $task = $validator->clean($_POST['user_task']);
            $validationFormData = $model->validationFormEditData($task);

            if ($validationFormData) {
                $updateDataDatabase = $model->update($task, $statusId, $id);
                if ($updateDataDatabase) {
                    Message::MessageSendSuccess(1, 'Данные успешно соxранены!');
                } else {
                    Message::MessageSendError(2, 'Данные не удалось сохранить. Попробуйте снова!');
                }
            }
        }

        $model = new Task;
        $get = intval($this->route['alias']);
        $findDataById = $model->getDataId($get);

        $this->set(compact('findDataById', 'model','get'));
    }

    public function viewAction()
    {
        $model = new Task;
        $get = intval($this->route['alias']);
        $findDataById = $model->getDataId($get);

        $this->set(compact('findDataById', 'model'));
    }

    /**
     * preview
     */
    public function ajaxAction()
    {
        $model = new Task();
        if($this->isAjax()) {
            $this->layout = false;
            $model->ajaxPreview();
        }
    }




}