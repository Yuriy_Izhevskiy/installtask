<?php
namespace app\controllers;

use app\models\Account;
use fw\libs\Message;
use fw\libs\Response;

class AccountController extends AppController
{
    public $layout = 'main';

    public function loginAction()
    {
        if (!empty($_POST)) {
            $admin = new Account();
            $response = new Response;

            if ($admin->login()) {
                $response->redirect('/');
            } else {
                Message::MessageSendError(2, 'Логин/пароль введены неверно!');
            }
            $response->redirect('/');
        }
    }

    public function logoutAction()
    {
        $response = new Response;
        $this->unsetSession();
        $response->redirect('/account/login/');
    }

    public function unsetSession()
    {
        if (isset($_SESSION['admin'])) {
            unset($_SESSION['admin']);
        }
    }
}