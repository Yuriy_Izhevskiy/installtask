<?php
namespace fw\libs;

class Response
{
    public function redirect($http = false)
    {
        if ($http) {
            $redirect = $http;
        } else {
            $redirect = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '/';
        }

        header('Location:'.$redirect);
    }
}