<?php
namespace fw\libs;

class Message
{
    const SUCCESS = 1;
    const ERROR = 2;
    const ATTENTION = 3;
    const ACCESS_RIGHTS = 4;

    /**
     * @param $item
     */
    public static function _MessageShow($item)
    {
        $item ?:  self::MessageSendSuccess(self::SUCCESS, 'Вы не внесли изменений, данные сохранены!');
    }

    /**
     * @param $p1 'Ошибка'
     * @param $p2 'Внимание'
     */
    public static function MessageSendError($p1, $p2)
    {
        if($p1 == self::ERROR) {
            $p1 = 'Ошибка';
        }
        if($p1 == self::ATTENTION) {
            $p1 = 'Внимание';
        }
        $_SESSION['message'] = '<div class = "mesage_block"><b>'.$p1.'</b>: ' .$p2. '</div>';
        exit(header('Location:' .$_SERVER['HTTP_REFERER']));
    }

    public static function MessageSendRole($p1, $p2)
    {
        if ($p1 == self::ACCESS_RIGHTS) {
            $p1 = 'Права доступа';
        }
        $_SESSION['message'] = '<div class = "mesage_block"><b>'.$p1.'</b>: ' .$p2. '</div>';
        exit(header('Location:' .'/'));
    }

    public static function MessageSendSuccess($p1, $p2)
    {
        if ($p1 == self::SUCCESS) {
            $p1 = 'Успех';
        }
        $_SESSION['message'] = '<div class = "alert alert-success"><b>'.$p1.'</b>: ' .$p2. '</div>';
        exit(header('Location:' .$_SERVER['HTTP_REFERER']));
    }

    public static function MessageShow()
    {
        $message = '';
        if ($_SESSION['message']){
            $message = $_SESSION['message'];
        }
        echo $message;
        $_SESSION['message'] = [];
    }

}