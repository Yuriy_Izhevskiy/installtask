<?php
namespace fw\libs\helpers;

abstract class Html
{
    const STATUS_UNDEFINED = '1';
    const STATUS_EXPIRED = '2';
    const STATUS_DONE = '3';
    const STATUS_PERFORMED = '4';

    public static function encode($content)
    {
        return htmlspecialchars($content, ENT_QUOTES | ENT_SUBSTITUTE, 'UTF-8', true);
    }

    public static function statusUndefined($param)
    {
        return Html::encode($param) === self::STATUS_UNDEFINED ? 'checked="checked"' : '';
    }

    public static function statusExpired($param)
    {
        return Html::encode($param) === self::STATUS_EXPIRED ? 'checked="checked"' : '';
    }

    public static function statusDone($param)
    {
        return Html::encode($param) === self::STATUS_DONE ? 'checked="checked"' : '';
    }

    public static function statusPerformed($param)
    {
        return Html::encode($param) === self::STATUS_PERFORMED ? 'checked="checked"' : '';
    }

}
