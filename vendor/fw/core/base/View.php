<?php

namespace fw\core\base;


class View
{
    public $route = [];
    public $view;
    public $layout;

    public function __construct($route, $layout = '',  $view = '')
    {
        $this->route = $route;
        if ($layout === false) {
            $this->layout = false;
        } else {
            $this->layout = $layout ?: LAYOUT;
        }

        $this->view = $view;

    }

    public function render($vars)
    {
        if (is_array($vars)) extract($vars);

        $fileView = APP . "/views/{$this->route['controller']}/{$this->view}.php";
        ob_start();

        if (is_file($fileView)) {
            require $fileView;
        } else {
            echo "<p>Не найден вид <b></b>{$fileView}</p>";
        }
        $content = ob_get_clean();

        if (false !== $this->layout) {
            $fileLayout = APP . "/views/layouts/{$this->layout}.php";
            if (is_file($fileLayout)) {
                require $fileLayout;
            } else {
                echo "<p>Не найден  шаблон <b></b>{$fileLayout}</p>";
            }
        }
    }
}