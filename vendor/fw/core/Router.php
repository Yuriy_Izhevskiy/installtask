<?php

namespace fw\core;

use fw\libs\Response;

class Router implements BasicMethods
{
    protected static $routes = [];
    protected static $route = [];

    /**
     * @param string $regexp
     * @param array $route
     */
    public static function addRoutes($regexp, array $route = [])
    {
        self::$routes[$regexp] = $route;
    }

    /**
     * @return array
     */
    public static function getRoutes()
    {
        return self::$routes;
    }

    /**
     * @return array
     */
    public static function getRoute()
    {
        return self::$route;
    }
    
    /**
     * ищет URL в таблице маршрутов
     * @param string $url входящий URL
     * @return boolean
     */
    public static function matchRoute($url)
    {
        foreach (self::$routes as $pattern => $route) {
            if (preg_match("#$pattern#i", $url, $matches)) {
                foreach ($matches as $key => $value) {
                    if (is_string($key)) $route[$key] = $value;
                }
                if (!isset($route['action'])) $route['action'] = 'index';
                $route['controller'] =  self::upperCamelCase($route['controller']);
                self::$route = $route;
                return true;
            }
        }
        return false;
    }

    /**
     * @param string $url
     */
    public static function dispatch($url)
    {
        $response = new Response;
        $url = self::removeQueryString($url);
        if (self::matchRoute($url)) {
            $controller = 'app\controllers\\' . self::$route['controller'] . 'Controller';
            if (class_exists($controller)) {
                $objController = new $controller(self::$route);
                $action = self::lowerCamelCase(self::$route['action'] . 'Action');
                if(method_exists($objController, $action)) {
                    $objController->$action();
                    $objController->getView();
                } else {
                    $response->redirect('/');
                  //  echo "Метод <b>$controller::$action</b> не найден";
                }
            } else {
                $response->redirect('/');
               //echo "Контроллер <b>$controller</b> не найден либо допущена ощибка в имени";
            }
        } else {
            http_response_code(404);
            include '404.html';
        }
    }

    /**
     * @param string $nameClass
     * @return mixed|string
     */
    public static function upperCamelCase($nameClass)
    {
        $nameClass = str_replace('-', ' ', $nameClass);
        $nameClass = ucwords($nameClass);
        $nameClass = str_replace(' ', '', $nameClass);
        return $nameClass;
       
    }

    /**
     * @param string $nameAction
     * @return string
     */
    public static function lowerCamelCase($nameAction)
    {
       return lcfirst(self::upperCamelCase($nameAction));
    }

    /**
     * @param string $url
     * @return string
     */
    public static function removeQueryString($url)
    {
        if ($url){
            $params = explode('&', $url);
            if (strpos($params[0], '=') === false) {
                return rtrim($params[0], '/');
            }else{
                return '';
            }
        }
    }

}
