<?php
namespace fw\core;

interface BasicMethods {

    /**
     * @param $regexp
     * @param array $route
     * @return mixed
     */
    public static function addRoutes($regexp, array $route = []);

    /**
     * @return mixed
     */
    public static function getRoutes();

    /**
     * @return mixed
     */
    public static function getRoute();

    /**
     * @param $url
     * @return mixed
     */
    public static function matchRoute($url);

    /**
     * @param $url
     * @return mixed
     */
    public static function dispatch($url);

    /**
     * @param $nameClass
     * @return mixed
     */
    public static function upperCamelCase($nameClass);

    /**
     * @param $nameAction
     * @return mixed
     */
    public static function lowerCamelCase($nameAction);

    /**
     * @param $url
     * @return mixed
     */
    public static function removeQueryString($url);
}
