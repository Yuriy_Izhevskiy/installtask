<?php

namespace fw\core;

class Db
{
    protected $pdo;
    protected static $instance;

    static $countSql = 0;

    const CONFIG_DB = ROOT . '/config/config_db.php';

    /**
     * @var array записывает все SQL запросы
     */
    static $querySql = [];

    /**
     * Db constructor.
     */
    protected function __construct()
    {
        $db = self::getConfigDB();
        $options = [
            \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
            \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
        ];

        try {
            $this->pdo = new \PDO($db['dns'], $db['user'], $db['password'], $options);
        } catch (PDOException $e) {
            echo 'Подключение не удалось: ' . $e->getMessage();
        }
    }


    public function getConfigDB()
    {
        return require_once self::CONFIG_DB;
    }

    /**
     * @return Db
     */
    public static function instance()
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }

    /**
     * @param $sql
     * @param array $params
     * @return bool
     */
    public function execute($sql, $params = [])
    {
        self::$countSql++;
        self::$querySql[] = $sql;
        $stmt = $this->pdo->prepare($sql);
        return $stmt->execute($params);
    }

    /**
     * @param $sql
     * @param array $params
     * @return array
     */
    public function query($sql, $params = [])
    {
        $this->countQuery($sql);

        $stmt = $this->pdo->prepare($sql);
        $result = $stmt->execute($params);
        if ($result !== false) {
            return $stmt->fetchAll(\PDO::FETCH_ASSOC);
        }
        return [];
    }

    /**
     * @param $sql
     * @param array $params
     * @return array|int
     */
    public function queryCount($sql, $params = [])
    {
        $this->countQuery($sql);

        $stmt = $this->pdo->prepare($sql);
        $result = $stmt->execute($params);

        if ($result !== false) {
            return $stmt->rowCount();
        }
        return [];
    }

    /**
     * @param $sql
     * @return array
     */
    public function countQuery($sql)
    {
        return [
            self::$countSql++,
            self::$querySql[] = $sql
        ];
    }
}